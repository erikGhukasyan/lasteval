<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
/**
 * Ce que fait ce fichier en 1 phrase
 *
 * Ce que fait ce fichier de façon détaillée
 *
 * @version ...
 */
/*
 * Copyright (C) 2019 Jean-Bernard HUET - Le Labo.VE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

?>

    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow, noarchive, nosnippet, noodp">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/style.css">

        <meta name="theme-color" content="#fafafa">
    </head>

    <body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->
<header id="header">
    <nav class="menu">
        <ul>
            <li>
                <a href="index.php" class="menu" title="Accueil">Accueil</a>
            </li>
            <li>
                <a href="" class="menu" title="Formations">Presentation</a>
            </li>
            <li>
                <a href="produits.php" class="menu" title="Audit">Liste Produit</a>
            </li>
            <?php
            if(!isset($_SESSION['email']) && empty($_SESSION['email'])) {
                ?>
                <li>
                    <a href="login.php" class="menu" title="Contact">Se connecter</a>
                </li>
                <?php
            }
            else {
                ?>
                <li>
                    <a href="membres.php" class="menu" title="Membre">Liste Membres</a>
                </li>

                <div class="dropdown">
                    <li><a href="profil.php" id="name_menu" class="dropbtn" title="user_name"><?php printf('%s', $_SESSION['email']); ?></a></li>

                    <div class="dropdown-content">
                            <a href="profil.php">Profil</a><br>
                            <a href="deconexion.php">Deconnexion</a>
                    </div>
                </div>


                <?php
            }
            ?>

        </ul>
    </nav>
</header>
<?php

