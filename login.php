<?php
if (!include_once 'header.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
?>
<div class="container">
    <div class="head_text">CONNEXION</div>
    <?php if(!empty($_GET['message'])) {?>
        <p class="error_msg"><?php printf('%s', $_GET['message']) ?></p>
    <?php }?>
    <form action="profil.php" method="post">

        <div>    <label for="email">Email</label>
            <input type="email" id="email" name="email" required /></div>


        <div>  <label for="pass">Mot de Pass</label>
            <input type="password" id="pass" name="pass" required minlength="8" maxlength="250" /></div>

        <button>Log in</button>
        <a class="a_inscription" href="index.php">Annuler</a>
    </form>
    <p>Pas encore membre ? <a href="inscription.php">Inscrivez-vous gratuitement</a></p>
</div>
<?php
if (!include_once 'footer.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
?>
