<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
include_once "functions.php";
include_once "basDeDonees.php";

if(isset($_POST) && !empty($_POST)) {
    $name = clean($_POST['name']);
    unset($_POST['name']);

    $surname = clean($_POST['surname']);
    unset($_POST['surname']);

    $dob = clean($_POST['dob']);
    unset($_POST['dob']);

    $tel = clean($_POST['tel']);
    unset($_POST['tel']);

    $email = clean($_POST['email']);
    unset($_POST['email']);

    $username = clean($_POST['pseudo']);
    unset($_POST['pseudo']);

    $pass = clean($_POST['pass']);
    unset($_POST['pass']);

    $confPass = clean($_POST['Cpass']);
    unset($_POST['Cpass']);

} else {
    navigate('inscription', "Vous devez remplir tous les champs obligatoires");
    exit;
}

if($pass != $confPass) {
    navigate('inscription', "Le mot de pass ne correspond pas");
    exit;
}

if (preg_match('~[0-9]+~', $name) || preg_match('~[0-9]+~', $surname)) {
    navigate('inscription', 'Le nom ou le prénom sont pas correct');
    exit;
}

foreach ($listOfUsers as $key => $value) {
    if($username === $value) {
        navigate('inscription', "Utilisateur avec le nom '$value' éxiste déjà");
        exit;
    }
    if($email === $key) {
        navigate('inscription', "Utilisateur avec le nom '$key' éxiste déjà");
        exit;
    }
}

$_SESSION['email'] = $email;

if (!include_once 'header.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
?>

<?php mail($email, 'Inscription', 'boooh'); ?>
<div class="container">
<p>Merci pour l'inscription. Un mail a été envoye à <?php printf('%s', $email); ?>:</p>
    <p><a href="index.php">ACCEUIL</a></p>
</div>
<?php
if (!include_once 'footer.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
?>
