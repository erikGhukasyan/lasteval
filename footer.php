<?php

/**
 * Ce que fait ce fichier en 1 phrase
 *
 * Ce que fait ce fichier de façon détaillée
 *
 * @version ...
 */
/*
 * Copyright (C) 2019 Jean-Bernard HUET - Le Labo.VE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

?>
<script src="js/vendor/modernizr-3.8.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write( '<script src="js/vendor/jquery-3.4.1.min.js"><\/script>' )</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<link rel="stylesheet" href="css/style.css">

<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<script>
    window.ga = function () {
        ga.q.push( arguments )
    };
    ga.q = [ ];
    ga.l = +new Date;
    ga( 'create', 'UA-XXXXX-Y', 'auto' );
    ga( 'set', 'transport', 'beacon' );
    ga( 'send', 'pageview' )
</script>
<script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

<footer>
    <a href="infoLegales.html" target="blank_">Informations légales</a>
    <a href="contact.php">Nous contacter</a>
    <a href="données.html" target="blank_">Politique de gestion des données</a>
</footer>


</html>
