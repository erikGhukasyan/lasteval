<?php
/**
 * Ce que fait ce fichier en 1 phrase
 *
 * Ce que fait ce fichier de façon détaillée
 *
 * @version ...
 */
/*
 * Copyright (C) 2019 Jean-Bernard HUET - Le Labo.VE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


if (!include_once 'header.php'):
?><!-- Mettre ici le code HTML5 pour l'entête par défaut --><?php
endif;
?>

<?php
if (!include_once 'footer.php'):
?><!-- Mettre ici le code HTML5 pour le pied de page par défaut --><?php
endif;
