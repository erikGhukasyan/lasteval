<?php
/**
 * Ce que fait ce fichier en 1 phrase
 *
 * Ce que fait ce fichier de façon détaillée
 *
 * @version ...
 */
/*
 * Copyright (C) 2019 Jean-Bernard HUET - Le Labo.VE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

if (!include_once 'header.php'):
?><!-- Mettre ici le code HTML5 pour l'entête par défaut --><?php
endif;
?>
<section id="contact">
    <form method="post" action="">
        <label>Pr&#xE9;nom *</label>
        <br/>
        <input name="firstname" type="text" value="" required="required" />
        <br/>
        <label>Nom *</label>
        <br/>
        <input name="name" type="text" value="" required="required" />
        <br/>
        <label>E-mail *</label>
        <br/>
        <input name="email" type="email" value="" required="required" pattern="^[a-z0-9._-]{3,50}@[a-z0-9.-]{3,50}\.[a-z]{2,4}$"/>
        <br/>
        <label>T&#xE9;l&#xE9;phone</label>
        <br/>
        <input name="tel" type="tel" value="" minlength="10" maxlength="10"/>
        <br/>
        <label>Sujet</label>
        <br/>
        <input name="subject" type="text" value="" />
        <br/>
        <label>Votre message *</label>
        <br/>
        <textarea name="message" maxlength="750" required="required"></textarea>
        <br/>
        <input name="contact" type="submit" value="Envoyer"/>
        <br/>
    </form>
</section>
<?php
if (!include_once 'footer.php'):
?><!-- Mettre ici le code HTML5 pour le pied de page par défaut --><?php
endif;
