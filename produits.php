<?php

/**
 * Ce que fait ce fichier en 1 phrase
 *
 * Ce que fait ce fichier de façon détaillée
 *
 * @version ...
 */
/*
 * Copyright (C) 2019 Jean-Bernard HUET - Le Labo.VE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


if (!include_once 'header.php') :
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --><?php
endif;
?>

    <h3>Liste des produits</h3>

    <div class="produits">
        <h4>MICRO</h4>
        <p><img src="img/001.jpg" alt="">Truc 1 pas cher ! <span><em>105€</em></span></p>
    </div>
    <div class="produits">
        <h4>ENCEINTES</h4>
        <p><img src="img/002.jpg" alt="">Truc 2 pas cher ! <span><em>1105€</em></span></p>
    </div>
    <div class="produits">
        <h4>MICRO</h4>
        <p><img src="img/003.jpg" alt="">Truc 3 pas cher ! <span><em>158€</em></span></p>
    </div>
    <div class="produits">
        <h4>MICRO</h4>
        <p><img src="img/004.jpg" alt="">Truc 4 pas cher ! <span><em>255€</em></span></p>
    </div>
    <div class="produits">
        <h4>CAISSE EN BOIS/METAL</h4>
        <p><img src="img/005.jpg" alt="">Truc 5 pas cher ! <span><em>685€</em></span></p>
    </div>
    <div class="produits">
        <h4>BATTERIE</h4>
        <p><img src="img/006.jpg" alt="">Truc 6 pas cher ! <span><em>1885€</em></span></p>
    </div>
    <div class="produits">
        <h4>CD DJ DREAM</h4>
        <p><img src="img/007.jpg" alt="">Truc 7 pas cher ! <span><em>1215€</em></span></p>
    </div>
    <div class="produits">
        <h4>ENCEINTES</h4>
        <p><img src="img/008.jpg" alt="">Truc 8 pas cher ! <span><em>1893€</em></span></p>
    </div>

<?php
if (!include_once 'footer.php') :
    ?>
    <!-- Mettre ici le code HTML5 pour le pied de page par défaut --><?php
endif;
