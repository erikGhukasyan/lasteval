<?php
if (!include_once 'header.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
?>
<div class="container">
    <div class="head_text">Inscription</div>
    <div class="description">Remplissez bien tous les champs obligatoires</div>
    <?php if(!empty($_GET['message'])) {?>
        <p class="error_msg"><?php printf('%s', $_GET['message']) ?></p>
    <?php }?>
    <form action="confirmation.php" method="post">
        <div> <label for="name">Prénom</label>
            <input type="text" id="name" name="name" required /></div>

        <div><label for="surname">Nom</label>
            <input type="text" id="surname" name="surname" required /></div>

        <div><label for="dob">Date de naissance</label>
            <input type="date" id="dob" name="dob" required /></div> <!--o	Au format JJ/MM/AAAA-->

        <div>  <label for="tel">Tél</label><input type="text" id="tel" name="tel" required /></div>

        <div>    <label for="email">Email</label>
            <input type="email" id="email" name="email" required /></div>

        <div>  <label for="pseudo">Pseudo</label>
            <input type="text" id="pseudo" name="pseudo" required minlength="8" maxlength="40" /></div>

        <div>  <label for="pass">Mot de Pass</label>
            <input type="password" id="pass" name="pass" required minlength="8" maxlength="250" /></div>

        <div>  <label for="Cpass">Confirmation du mot de passe</label>
            <input type="password" id="Cpass" name="Cpass" required minlength="8" maxlength="250" /></div>

        <button>Inscrire</button>
        <a class="a_inscription" href="index.php">Annuler</a>
    </form>

</div>
<?php
if (!include_once 'footer.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
?>
