<?php
    $listOfUsers = [
        "john@google.fr" => "johnygrence",
        "paola@yahoo.fr" => "pipigniorz",
        "michael@google.fr" => "mimilieneze",
        "rachel@google.fr" => "rabiteleqs",
        "lola@google.fr" => "lololifdsk",
    ];

    $listOfPass = [
        "john@google.fr" => "1234azerqsdfwxcv",
        "paola@yahoo.fr" => "qsdfghjklm",
        "michael@google.fr" => "wxcvbn",
        "rachel@google.fr" => "azertyuiop",
        "lola@google.fr" => "123456789",
    ];
?>