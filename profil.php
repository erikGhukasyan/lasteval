<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
include_once "functions.php";
include_once "basDeDonees.php";


if(!isset($_SESSION['email']) && empty($_SESSION['email'])) {
    if (isset($_POST) && !empty($_POST)) {
        $email = clean($_POST['email']);
        unset($_POST['email']);

        $pass = clean($_POST['pass']);
        unset($_POST['pass']);

    } else {
        navigate('login', "Vous devez remplir tous les champs obligatoires");
    }


    foreach ($listOfPass as $key => $value) {
        if($email === $key && $pass === $value) {
            $_SESSION['loggedin'] = true;
            $_SESSION['email'] = $email;
            break;
        }
    }
}
if (!include_once 'header.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
if(isset($_SESSION['email']) && !empty($_SESSION['email'])) {
    ?>
    <p>Welcome to the member's area, <?php printf('%s', $_SESSION['email']); ?> "!"</p>
    <a href="deconexion.php">DECONNECTION</a>
    <?php
} else {
    navigate('login', "Username or password is incorrect");
    exit;
}


if (!include_once 'footer.php'):
    ?>
    <!-- Mettre ici le code HTML5 pour l'entête par défaut --> <?php
endif;
?>
